package laboproject.ozouaki.instamusic;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import laboproject.ozouaki.instamusic.common.model.User;
import laboproject.ozouaki.instamusic.common.request.ResultCallback;
import laboproject.ozouaki.instamusic.common.request.UserRequestService;
import laboproject.ozouaki.instamusic.connexion.MainBottomNavigationActivity;
import laboproject.ozouaki.instamusic.connexion.ResetPasswordActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, ResultCallback {

    private EditText writeMail;
    private EditText writeUsername;
    private EditText writePsw;
    private Button btnSignIn;
    private Button btnLogin;
    private Button btnReset;
    private ImageView spin;
    private RotateAnimation rotate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.abc_fade_in,R.anim.abc_fade_out);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        //initialisation
        writeMail = findViewById(R.id.email);
        writeUsername = findViewById(R.id.username);
        writePsw = findViewById(R.id.psw);
        btnSignIn = findViewById(R.id.btn_sign);
        btnSignIn.setOnClickListener(this);
        btnLogin = findViewById(R.id.btn_log);
        btnLogin.setOnClickListener(this);
        btnReset = findViewById(R.id.btn_reset_password);
        btnReset.setOnClickListener(this);
        spin = findViewById(R.id.spinLogo);

        //Animation for a progressBar look like trick
        rotate = new RotateAnimation(0, 1080, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setDuration(10000);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_sign:
                signIn();
                break;
            case R.id.btn_log:
                logIn();
                break;
            case R.id.btn_reset_password:
                forgot();
                break;
        }
    }

    private void signIn(){
        String email = writeMail.getText().toString().trim();
        String username = writeUsername.getText().toString().trim();
        final String password = writePsw.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(getApplicationContext(), "Enter your email address!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(username)) {
            Toast.makeText(getApplicationContext(), "Enter your username", Toast.LENGTH_SHORT).show();
            return;
        }

        //format mail address
        if (!email.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
            Toast.makeText(getApplicationContext(), "Enter a valid mail address!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(getApplicationContext(), "Enter your password!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (password.length() < 6) {
            Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
            return;
        }

        spin.startAnimation(rotate);

        final User user = new User(username, email);

        UserRequestService.getInstance().SignIn(email, password, this);
    }

    private void logIn() {
        String email = writeMail.getText().toString().trim();
        final String password = writePsw.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(getApplicationContext(), "Enter your right email address!", Toast.LENGTH_SHORT).show();
            return;
        }

        //format mail address
        if (!email.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
            Toast.makeText(getApplicationContext(), "Enter a valid mail address!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            Toast.makeText(getApplicationContext(), "Enter your password!", Toast.LENGTH_SHORT).show();
            return;
        }

        if (password.length() < 6) {
            Toast.makeText(getApplicationContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
            return;
        }

        spin.startAnimation(rotate);

        UserRequestService.getInstance().LogIn(email, password, this);
    }

    private void forgot() {
        startActivity(new Intent(MainActivity.this, ResetPasswordActivity.class));
        finish();
    }

    @Override
    public void onRequestFinished(String obj) {
        startActivity(new Intent(MainActivity.this, MainBottomNavigationActivity.class));
        Toast.makeText(getApplicationContext(), "Welcome", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    public void onChangeFinished(String obj) {

    }

    @Override
    public void onDataChange(Object obj) {

    }

    @Override
    public void onError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }

}

