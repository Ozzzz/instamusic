package laboproject.ozouaki.instamusic.common.request;

/**
 * Created by Ozouaki Albert on 22/11/2017.
 */

public interface ResultCallback<T> {
    void onRequestFinished(String obj);
    void onChangeFinished(String obj);
    void onDataChange(T obj);
    void onError(String error);
}
