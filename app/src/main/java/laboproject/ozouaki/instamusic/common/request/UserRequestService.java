package laboproject.ozouaki.instamusic.common.request;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import laboproject.ozouaki.instamusic.common.model.User;

/**
 * Created by Ozouaki Albert on 20/12/2017.
 */

public class UserRequestService {

    private static UserRequestService SINGLETON;
    private FirebaseDatabase database;
    private DatabaseReference dbUserRef;
    private FirebaseAuth auth;
    private FirebaseUser user;

    private String UsersRef = "user";

    private static final Object __synchronizedObject = new Object();

    public static UserRequestService getInstance() {
        if (SINGLETON == null) {
            synchronized (__synchronizedObject) {
                if (SINGLETON == null) {
                    SINGLETON = new UserRequestService();
                }
            }
        }
     return SINGLETON;

    }

    private UserRequestService() {
        auth = FirebaseAuth.getInstance();
        user = auth.getCurrentUser();
        database = FirebaseDatabase.getInstance();
        dbUserRef = database.getReference(UsersRef);
    }

    public User getCurrentUser() {
        return toUser(auth.getCurrentUser());
    }

    private User toUser(FirebaseUser u) {
        if (u == null)
            return null;
        User tempUser = new User();
        tempUser.setUsername(u.getDisplayName());
        return tempUser;
    }

    // DATABASE

    public void AddUser(User user){
        user.setUid(dbUserRef.push().getKey());
        dbUserRef.child(user.getUid()).setValue(user);
    }

    public void GetUsers(final ResultCallback<ArrayList<User>> callback){
        // On commence à écouter, Firebase ouvre un nouveau thread
        dbUserRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Firebase nous renvois un dictionnaire (hashMap) de clé/valeur
                // On doit donc récupérer notre objet, puis la liste de produits
                GenericTypeIndicator<HashMap<String, User>> objectsGTypeInd = new GenericTypeIndicator<HashMap<String, User>>() {};

                //On récupère notre hashmap
                Map<String, User> objectHashMap = dataSnapshot.getValue(objectsGTypeInd);
                ArrayList<User> objectArrayList = new ArrayList<User>();
                if(objectHashMap != null){
                    // On récup la valeur de notre hashmap
                    objectArrayList.addAll(objectHashMap.values());
                    int i = 0;
                    // On parcours notre hashmap, afin de récupérer chaques références des produits, utilisé pour faire une maj ou le delete
                    for ( String key : objectHashMap.keySet() ) {
                        objectArrayList.get(i).setUid(key);
                        i++;
                    }
                }
                // On appelle notre callback
                callback.onDataChange(objectArrayList);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("Error Firebase", databaseError.getMessage());
                callback.onError(databaseError.getMessage());
            }
        });
    }

    public void RemoveUser(String uid){
        dbUserRef.child(uid).removeValue();
    }

    // AUTHENTIFICATION

    public void SignIn(String email, String password, final ResultCallback callback) {
        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (!task.isSuccessful()) {
                    AddUser(toUser(user));
                    callback.onError("Authentication failed!");
                } else {
                    callback.onRequestFinished("");
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.onError(e.getMessage());
            }
        });
    }

    public void LogIn(String email, String password, final ResultCallback callback) {
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (!task.isSuccessful()) {
                    callback.onError("Authentication failed!");
                } else {
                    callback.onRequestFinished("");
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.onError(e.getMessage());
            }
        });
    }

    public void SignOut() {
        auth.signOut();
    }

    public void Reset(String email, final ResultCallback callback) {
        auth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    callback.onRequestFinished("");
                } else {
                    callback.onError("Failed to send reset email!");
                }
            }
        });
    }

    public void Delete() {
        user.delete();
    }

}
