package laboproject.ozouaki.instamusic.common.model;

/**
 * Created by Ozouaki Albert on 22/11/2017.
 */

public class User {
    private String username;
    private String email;
    private String uid;

    public User() {
    }

    public User(String username, String email) {
        this.username = username;
        this.email = email;
    }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getEmail() { return email; }

    public void setEmail(String email) { this.email = email; }

    public String getUid() { return uid; }

    public void setUid(String uid) { this.uid = uid; }

    @Override
    public int hashCode() {
        int result = username != null ? username.hashCode() : 0;
        result = 31 * result + (uid != null ? uid.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", uid='" + uid + '\'' +
                "}";
    }
}
