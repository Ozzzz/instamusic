package laboproject.ozouaki.instamusic.connexion;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import laboproject.ozouaki.instamusic.R;

public class SearchFragment extends Fragment {

    public SearchFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_search_fragment, container, false);
    }
}
