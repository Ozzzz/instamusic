package laboproject.ozouaki.instamusic.connexion;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import laboproject.ozouaki.instamusic.MainActivity;
import laboproject.ozouaki.instamusic.R;
import laboproject.ozouaki.instamusic.common.request.ResultCallback;
import laboproject.ozouaki.instamusic.common.request.UserRequestService;

public class ResetPasswordActivity extends AppCompatActivity implements View.OnClickListener, ResultCallback {

    private EditText writeMail;
    private Button resetPassword;
    private Button back;
    private ImageView spin;
    private RotateAnimation rotate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.abc_fade_in,R.anim.abc_fade_out);
        setContentView(R.layout.activity_reset_password);
        init();
    }

    private void init() {
        //initialisation
        writeMail = findViewById(R.id.email);
        resetPassword = findViewById(R.id.btnReset);
        resetPassword.setOnClickListener(this);
        back = findViewById(R.id.btnBack);
        back.setOnClickListener(this);
        spin = findViewById(R.id.spinLogo);

        //Animation for a progressBar look like trick
        rotate = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotate.setInterpolator(new LinearInterpolator());
        rotate.setDuration(10000);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnBack:
                back();
                break;
            case R.id.btnReset:
                reset();
                break;
        }
    }

    private void back() {
        spin.startAnimation(rotate);

        startActivity(new Intent(ResetPasswordActivity.this, MainActivity.class));
        finish();
    }

    private void reset() {
        String email = writeMail.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            Toast.makeText(getApplicationContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
            return;
        }

        //format mail address
        if (!email.matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")) {
            Toast.makeText(getApplicationContext(), "Enter a valid mail address!", Toast.LENGTH_SHORT).show();
            return;
        }

        spin.startAnimation(rotate);

        UserRequestService.getInstance().Reset(email, this);

    }

    @Override
    public void onRequestFinished(String obj) {
        finish();
    }

    @Override
    public void onChangeFinished(String obj) {

    }

    @Override
    public void onDataChange(Object obj) {

    }

    @Override
    public void onError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }
}
