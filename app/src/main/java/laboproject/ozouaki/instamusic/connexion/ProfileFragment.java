package laboproject.ozouaki.instamusic.connexion;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import laboproject.ozouaki.instamusic.MainActivity;
import laboproject.ozouaki.instamusic.R;
import laboproject.ozouaki.instamusic.common.model.User;
import laboproject.ozouaki.instamusic.common.request.ResultCallback;
import laboproject.ozouaki.instamusic.common.request.UserRequestService;

public class ProfileFragment extends Fragment implements View.OnClickListener, ResultCallback {

    private Button signOut;
    private Button deleteUser;
    private TextView usernameChange;

    public ProfileFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_profile_fragment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init(view);
        initUsername(view);
    }

    private void init(View view) {
        signOut = view.findViewById(R.id.btn_sign_out);
        signOut.setOnClickListener(this);
        deleteUser = view.findViewById(R.id.btn_delete_user);
        deleteUser.setOnClickListener(this);
        usernameChange = view.findViewById(R.id.user_profile_name);
    }

    private void initUsername(View view) {
        User user = UserRequestService.getInstance().getCurrentUser();
        usernameChange.setText(user.getUsername());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_sign_out:
                signOut();
                break;
            case R.id.btn_delete_user:
                deleteAccount();
                break;
        }
    }

    private void signOut() {
        UserRequestService.getInstance().SignOut();
        startActivity(new Intent(getContext(), MainActivity.class));
        getActivity().finish();
    }

    private void deleteAccount() {
        //DialogFragment
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Confirm ?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User confirm to do it
                UserRequestService.getInstance().Delete();
                Toast.makeText(getContext(), "We will miss you :(", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getContext(), MainActivity.class));
                getActivity().finish();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        // Create the AlertDialog object and show it
        builder.create();
        builder.show();
    }

    @Override
    public void onRequestFinished(String obj) {
        getActivity().finish();
    }

    @Override
    public void onChangeFinished(String obj) {

    }

    @Override
    public void onDataChange(Object obj) {

    }

    @Override
    public void onError(String error) {
        Toast.makeText(getContext(), error, Toast.LENGTH_LONG).show();
    }
}
